package com.wks.bookstore.respositories;

import com.wks.bookstore.models.Bookmark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

/**
 * Created by waqqas on 10/7/16.
 */
public interface BookmarkRepository extends JpaRepository<Bookmark,Long>{
    Collection<Bookmark> findByAccountUsername(String username);
}
