package com.wks.bookstore.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

/**
 * Created by waqqas on 10/7/16.
 */
@Entity
public class Account {


    public String username;
    @JsonIgnore
    public String password;
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(mappedBy = "account")
    private Set<Bookmark> bookmarks;

    private Account(){
        //JPA
    }

    public Account(String name, String password){
        this.username = name;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }


    public Long getId() {
        return id;
    }

    public Set<Bookmark> getBookmarks() {
        return bookmarks;
    }

}
