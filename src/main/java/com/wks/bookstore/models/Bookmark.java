package com.wks.bookstore.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by waqqas on 10/7/16.
 */
@Entity
public class Bookmark {


    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Account account;

    public String uri;
    public String description;

    public Bookmark(){
        //JPA
    }

    public Bookmark(Account account, String uri, String description){
        this.account = account;
        this.uri = uri;
        this.description= description;
    }

    public Long getId() {
        return id;
    }

    public Account getAccount() {
        return account;
    }

    public String getUri() {
        return uri;
    }

    public String getDescription() {
        return description;
    }
}

