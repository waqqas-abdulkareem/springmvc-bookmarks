package com.wks.bookstore.controllers;

import com.wks.bookstore.exceptions.UserNotFoundException;
import com.wks.bookstore.models.Account;
import com.wks.bookstore.models.Bookmark;
import com.wks.bookstore.respositories.AccountRepository;
import com.wks.bookstore.respositories.BookmarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by waqqas on 10/7/16.
 */
@RestController
@RequestMapping("api/users/{userId}/bookmarks")
public class BookmarksController {

    private final BookmarkRepository bookmarkRepository;

    private final AccountRepository accountRepository;

    @Autowired
    BookmarksController(BookmarkRepository bookmarkRepository,
                           AccountRepository accountRepository) {
        this.bookmarkRepository = bookmarkRepository;
        this.accountRepository = accountRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<Void> add(@PathVariable String userId, @RequestBody final Bookmark input) throws UserNotFoundException{
        this.validateUser(userId);
        return this.accountRepository.findByUsername(userId).map(new Function<Account, ResponseEntity>() {
            @Override
            public ResponseEntity apply(Account account) {
                Bookmark result = bookmarkRepository.save(new Bookmark(account,input.getUri(),input.getDescription()));

                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.setLocation(ServletUriComponentsBuilder
                        .fromCurrentRequest().path("/{id}")
                        .buildAndExpand(result.getId()).toUri());
                return new ResponseEntity<Void>(null, httpHeaders, HttpStatus.CREATED);
            }
        }).get();
    }

    @RequestMapping(value="/{bookmarkId}", method = RequestMethod.GET)
    Bookmark readBookmark(@PathVariable String userId, @PathVariable Long bookmarkId) throws UserNotFoundException{
        this.validateUser(userId);
        return this.bookmarkRepository.findOne(bookmarkId);
    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<Bookmark> readBookmarks(@PathVariable String userId) throws UserNotFoundException{
        this.validateUser(userId);
        return this.bookmarkRepository.findByAccountUsername(userId);
    }

    private void validateUser(final String userId) throws UserNotFoundException{
        this.accountRepository.findByUsername(userId).orElseThrow(new Supplier<UserNotFoundException>() {
            @Override
            public UserNotFoundException get() {
                return new UserNotFoundException(userId);
            }
        });
    }
}
