package com.wks.bookstore.controllers;

import com.wks.bookstore.exceptions.UserNotFoundException;
import com.wks.bookstore.models.Account;
import com.wks.bookstore.models.Bookmark;
import com.wks.bookstore.respositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.function.Function;

/**
 * Created by waqqas on 10/9/16.
 */
@RestController
@RequestMapping("api/users/")
public class AccountController {

    private AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    Account add(@RequestBody final Account input) {
        return this.accountRepository.save(new Account(input.username, input.password));
    }
}
